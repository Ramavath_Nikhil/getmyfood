<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //
	public function __construct(){

	}

	
	public function showPath(Request $request){
		$uri = $request->path();
		echo '<br>URI: '.$uri;
		
		$url = $request->url();
		echo '<br>';
		
		echo 'URL: '.$url;
		$method = $request->method();
		echo '<br>';
		
		echo 'Method: '.$method;
	}


	public function postRegister(Request $request){
      //Retrieve the name input field
		$first_name = $request->input('first_name');
		$last_name = $request->last_name;
		$email = $request->email;
		$mobile = $request->mobile;
		$password = $request->password;
		$confirm_password = $request->confirm_password;

		if(empty($first_name))
		{

			$this->goingBack("Please enter First Name");

		}

		else
		{

			if(empty($last_name))
			{

				$this->goingBack("Please enter Last Name<br/>");

			}

			else
			{

				if(empty($email))
				{
					$this->goingBack("Please enter Email"); 
				}

				else
				{

					if(empty($mobile))
					{
						$this->goingBack("Please enter Mobile");
					}

					else
					{
						if(empty($password))
						{
							$this->goingBack("Please enter password");
						}

						else
						{

							if(empty($confirm_password))
							{
								$this->goingBack("Please enter Confirm password");
							}

							else
							{


    // check if e-mail address is well-formed
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$this->goingBack("Please enter valid email id");
								}

								else
								{
if(preg_match('/^\d{10}$/',$mobile)) // phone number is valid
{

	if($password == $confirm_password)
	{

		$myFile = "registration_details.txt";
		$message = "First Name: ".$first_name.PHP_EOL."Last Name: ".$last_name.PHP_EOL."Email: ".$email.PHP_EOL."Mobile: ".$mobile.PHP_EOL."password: ".$password.PHP_EOL;
		if (file_exists($myFile)) {
			$fh = fopen($myFile, 'a');
			fwrite($fh, $message.PHP_EOL);
		} else {
			$fh = fopen($myFile, 'w');
			fwrite($fh, $message.PHP_EOL);
		}
		fclose($fh);

		echo "You have registered successfully<br/>";
		echo '<a href = "/">Click Here</a> to go back.';

	}
	else
	{

		$this->goingBack("Password and confirm password should be same");
	}





}
    else // phone number is not valid
    {

    	$this->goingBack("Phone number invalid");
    }




}
}
}
}
}
}


}










     /* echo "Record inserted successfully.<br/>";
     echo '<a href = "/insert">Click Here</a> to go back.';*/


 }




 public function goingBack($message)
 {
 	echo '<script language="javascript">';
 	echo 'alert("'.$message.'");  window.history.back();';
 	echo '</script>';
 }

}


