<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>welcome | GetMyFood</title>
       
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <style>
            .carousel-inner > .item > img,
            .carousel-inner > .item > a > img {
                width: 100%;
                margin: auto;
                height: 60vh;
            }
        </style>
        <script type="text/javascript">
            $(document).on('click', '.login_btn', function () {
                $('.login_sec').slideDown(300);
            });
            $(document).on('click', '.reg_btn', function () {
                $('.reg_sec').slideDown(300);
            });
            $(document).on('click', '.cls_log', function () {
                $('.login_sec').slideUp(300);
            });
            $(document).on('click', '.cls_reg', function () {
                $('.reg_sec').slideUp(300);
                $('.login_sec').hide();

            });
        </script>
    </head>
    <body>

        <div class="container">
            <div class="top_header">
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <div class="brand_section">
                            <h1>GetMyFood</h1>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                        <div class="log_reg">
                            <button type="button" class="login_btn">Login</button>
                            <button type="button" class="reg_btn">Register</button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="navgation_section">
                <ul>
                    <li><a>Home</a></li>
                    <li><a>About</a></li>
                    <li><a>Menu</a></li>
                    <li><a>Contact</a></li>
                </ul>
            </div>
            <div class="carousal_section">
                <div class="row">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="images/slide/banner1.jpg" alt="No Image" width="460" height="345">
                            </div>

                            <div class="item">
                                <img src="images/slide/banner2.jpg" alt="No Image" width="460" height="345">
                            </div>

                            <div class="item">
                                <img src="images/slide/banner3.jpg" alt="No Image" width="460" height="345">
                            </div>

                           
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="new_items_section">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="late_new">
                            <img src="images/menuicon.png" height="80" width="80" />
                             </br>
                            <h1><p>Choose Food</p></h1>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="late_new">
                            <img src="images/dollaricon.png" height="80" width="80"/>
                            </br>
                            <h1><p>Pay Online</p></h1>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="late_new">
                            <img src="images/pizzaicon.png" height="80" width="80"/>
                            </br>
                            <h1><p>Enjoy Food</p></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contact_section">
                            <h4>500 Terry Francois St.</h4>
                            <h4>San Francisco, CA 94158 </h4>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="contact_section">
                            <img src="images/fb.png" alt="Facebook"/>
                            <img src="images/twitter.png" alt="Facebook"/>
                            <img src="images/instagram.png" alt="Facebook"/>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal_login reg_sec" style="display: none">
            <div class="modal_body">
                <div class="modal_head">
                    Register
                </div>
                <div class="modal_content">

                  <form action = "/user/register" method = "post">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group fr_one">
                                <label>First name</label>
                                <input type="text" name = "first_name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group fr_one">
                                <label>Last name</label>
                                <input type="text" name = "last_name" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Mobile</label>
                                <input type="text" name = "mobile" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="email" name = "email" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name = "password" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Confirm password</label>
                                <input type="password" name="confirm_password" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-6 pad_no">
                                <div class="form-group sub_sec">
                                    <button type="button" class="login_btn cls_reg">Close</button>
                                </div>
                            </div>
                            <div class="col-sm-6 pad_no">
                                <div class="form-group sub_sec">
                                    <button type = "submit"  class="reg_btn">Register</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal_login login_sec" style="display: none">
            <div class="modal_body">
                <div class="modal_head">
                    login
                </div>
                <div class="modal_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group fr_one">
                                <label>Username</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group fr_one">
                                <label>Password</label>
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6">
                            <div class="col-sm-6 pad_no">
                                <div class="form-group sub_sec">
                                    <button type="button" class="login_btn cls_log">Close</button>
                                </div>
                            </div>
                            <div class="col-sm-6 pad_no">
                                <div class="form-group sub_sec">
                                    <button type="button" class="reg_btn">login</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
